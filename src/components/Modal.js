import React from "react";
import { Button } from "./Button";

export class Modal extends React.Component {
    render() {
        return (
            <div
            style ={{
                backgroundColor: this.props.backgroundColor,
                width: this.props.maxWidth,
                height: this.props.maxHeight,
                color: this.props.color,
                position: this.props.posRelative,
                margin: this.props.margin,
                borderRadius: this.props.radius,

            }}>
                <h1
                    style={{
                        paddingLeft: "20px",
                        backgroundColor: "#dd2c00",
                        height: "80px",
                        fontSize: "26px",
                        display: "flex",
                        justifyContent: "flex-start",
                        alignItems: "center",
                        borderTopRightRadius: "6px",
                        borderTopLeftRadius: "6px",



                    }}
                >{this.props.header}
                </h1>
                <div
                    style={{
                        position: "absolute",
                        top: "18px",
                        right: "30px",
                        fontSize: "30px",
                        cursor: "pointer"
                    }}
                onClick={(e) =>{
                    this.props.onClose()
                }}
                >X</div>
                <p
                style={{
                    marginTop: "50px"
                }}>{this.props.textFirst}
                    </p>
                <p
                >{this.props.textSecond}</p>
                <div
                    style={{
                        marginTop: "30px"
                    }}>
                    {this.props.actions}
                </div>
            </div>
        )
    }
}