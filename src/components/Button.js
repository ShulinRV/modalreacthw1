import React from "react";

export class Button extends React.Component{
    render() {
        return <button
            className="btn"
            style = {{
                backgroundColor: this.props.backgroundColor,
                color: this.props.textColor,
                borderRadius: this.props.borderRad,
                margin: this.props.marginLeft
            }}
            onClick={(e) => {
        this.props.handleClick()
        }}>{this.props.text}
        </button>
    }
}