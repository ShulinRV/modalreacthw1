import React from "react";


export class SecondButton extends React.Component{
    render() {
        return <button
        className="secondBtn"
        style={{
            backgroundColor: this.props.backgroundColorBtn,
            border: this.props.border,
            color: this.props.color,
            width: this.props.width,
            height: this.props.height,
            margin: this.props.marginLeft,
            cursor: this.props.cursor,
        }}
        onClick={(e)=>{
            this.props.onClose()
        }
        }>{this.props.text}
        </button>


        }

}