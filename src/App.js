import React from "react";
import './App.css';
import {Button} from "./components/Button.js";
import {Modal} from "./components/Modal";
import {SecondButton} from "./components/secondButton";

class App extends React.Component {
    state = {isModalVisibleFirst: false, isModalVisibleSecond: false}
  render() {
    return (
        <div className="App"
        >
          <Button
          text="Open first modal"
          backgroundColor = "red"
          textColor = "white"
          borderRad = "5px"
          marginLeft = "10px"
          handleClick={(e) => {
            this.setState({isModalVisibleFirst: true})
          }} />
            <Button
                text="Open second modal"
                backgroundColor = "black"
                textColor = "white"
                borderRad = "5px"
                marginLeft = "10px"
                handleClick={(e) => {
                    this.setState({isModalVisibleSecond: true})
                }}
            />
            {this.state.isModalVisibleFirst && (<Modal
                radius = "6px"
                margin = "0 auto"
                maxWidth = "600px"
                maxHeight = "300px"
                posRelative = "relative"
                color = "white"
                backgroundColor = {"#f4511e"}
                header="Do you want to delete this file?"
                textFirst="Once you delete this file, it won't be possible to undo this action."
                textSecond="Are you sure you want to delete it?"
            onClose={() =>{this.setState({isModalVisibleFirst: false})}}
                actions={
                    <>{this.state.isModalVisibleFirst && (<SecondButton
                        backgroundColorBtn = "rgba(0, 0, 0, 0.2)"
                        cursor = "pointer"
                        border = "none"
                        color = "white"
                        width = "80px"
                        height = "30px"
                        marginLeft = "10px"
                        text="Ok"
                        onClose={() =>{
                            this.setState({isModalVisibleFirst: false})}}/>)}
                        {this.state.isModalVisibleFirst && (<SecondButton
                            backgroundColorBtn = "rgba(0, 0, 0, 0.2)"
                            border = "none"
                            color = "white"
                            width = "80px"
                            height = "30px"
                            text="Cancel"
                            cursor = "pointer"
                            onClose={() =>{
                                this.setState({isModalVisibleFirst: false})}}/>)}
                    </>}/>)}
            {this.state.isModalVisibleSecond && (<Modal
                radius = "6px"
                margin = "0 auto"
                maxWidth = "600px"
                maxHeight = "300px"
                posRelative = "relative"
                color = "white"
                backgroundColor = {"#f4511e"}
                header="Do you want to delete this file?"
                textFirst="Once you delete this file, it won't be possible to undo this action."
                textSecond="Are you sure you want to delete it?"
                onClose={() =>{this.setState({isModalVisibleSecond: false})}}
                actions={
                    <>{this.state.isModalVisibleSecond && (<SecondButton
                        backgroundColorBtn = "rgba(0, 0, 0, 0.2)"
                        cursor = "pointer"
                        border = "none"
                        color = "white"
                        width = "80px"
                        height = "30px"
                        marginLeft = "10px"
                        text="Ok"
                        onClose={() =>{
                            this.setState({isModalVisibleSecond: false})}}/>)}
                        {this.state.isModalVisibleSecond && (<SecondButton
                            text="Cancel"
                            cursor = "pointer"
                            backgroundColorBtn = "rgba(0, 0, 0, 0.2)"
                            border = "none"
                            color = "white"
                            width = "80px"
                            height = "30px"
                            onClose={() =>{
                                this.setState({isModalVisibleSecond: false})}}/>)}
                    </>}/>)}




  </div>
)}}

export default App;
